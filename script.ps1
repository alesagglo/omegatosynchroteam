﻿# Script CBH-1
<#
.SYNOPSIS
     Envoi vers Syncrhoteam des modifications 
.DESCRIPTION
     Le dépot de fichier au format MS Office ne focntionne pas lorsque l'appel se fait depuis une tache planifiée.
     Dans ce cas il faut utiliser du CSV.
.NOTES
     Author     : Acelys pour Alès Agglomération
     License    : GPLv3
.LINK
     
#>
$scriptPath = $MyInvocation.MyCommand.Path
$scriptDir = Split-Path $scriptPath
Push-Location $scriptDir
[Environment]::CurrentDirectory = $scriptDir

Import-Module ".\crudapi.psm1" -Force 
Import-Module ".\functions.psm1" -Force

$LogFile = "$scriptDir\data\logs\proc_$env:computername.log"
if (Test-Path $LogFile) 
{
  Remove-Item $LogFile
}

$dateTraitement = Get-Date -Format "yyyy-MM-dd"

#######################################################################################
# Script permettant la synchronisation des données OMEGA dans Synchroteam
#######################################################################################

$path = Get-Location 

#On récupère les paramètres de configuration
$config = Get-Content "$path\config.json" | Out-String | ConvertFrom-Json

WriteLog -LogFile $LogFile -LogString "Lecture configuration:"
$config
$config.champs_personnalises

#Configuration
$tokenApi = $config.token_api
$domaine = $config.domaine
$bearer=[Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes("${domaine}:$tokenApi"))

#chemin pour localiser les fichiers à traiter ainsi que l'écriture des fichiers en sortie
$pathError = "$path\data\recyclage"
$pathOK = "$path\data\traites"
$pathLog = "$path\data\logs"
$pathTmp = "$path\data\tmp"
$pathData = "$path\data"

New-Item -Path "$path\data\" -Name "tmp" -ItemType "directory" -ErrorAction Continue
Move-Item -Path "$pathError\*.*" -Destination "$path\data" -Force

$compteurTraite = 0
$compteurARecycler = 0
$compteurLu = 0

#la nouvelle source étant en xlsx plutot que csv, on ajoute un prétraitement transformant le xlsx en csv
if($false){
$files = Get-ChildItem -Path "$path\data" -Filter '*.xls*'
foreach ($file in $files.FullName){
    ExcelToCsv -file $file
    Move-Item -Path $file -Destination "$pathOK" -Force
}
}

$files = Get-ChildItem -Path "$path\data" -Filter '*.csv'
foreach ($file in $files.FullName){
    $filename = "$file".Split("\")[ "$file".Split("\").Count - 1]
    WriteLog -LogFile $LogFile -LogString "Traitement du fichier $filename"
    Get-Content $file | Out-File -FilePath "$pathTmp\${dateTraitement}_$filename" -Encoding utf8
    $csv = Import-Csv -Delimiter ";" -Path "$pathTmp\${dateTraitement}_$filename" -Encoding Unicode

    foreach ($enreg In $csv){
        if ([String]::IsNullOrEmpty($enreg.'N° du Point de Consommation (PC)')){
            WriteLog -LogFile $LogFile -LogString "Le numéro de point de consommation est null"
        } else {
            $compteurLu = $compteurLu + 1
            if ($compteurTraite -lt $config.limite_quotidienne) {
                $response = Post-Synchroteam -data $enreg -date $dateTraitement -bearer $bearer -config $config -file "$pathLog\${dateTraitement}_$filename.log"
                $compteurTraite = $compteurTraite + 1
            } else {
                #on stoppe les synchros, et on écrit les enregistrements restants dans un fichier à retraiter le lendemain.
                Recycle-Enreg -file "$pathError\${dateTraitement}_$filename" -data $enreg
                $compteurARecycler = $compteurARecycler + 1
            }
        }
    }
    Move-Item -Path $file -Destination "$pathOK\${dateTraitement}_$filename" -Force
}

Purge-File -retention $config.duree_retention -pathTmp $pathTmp -pathLog $pathLog -pathOk $pathOK -pathData $pathData

WriteLog -LogFile $LogFile -LogString "Compteur lu: $compteurLu"
WriteLog -LogFile $LogFile -LogString "Traité: $compteurTraite / A Recycler: $compteurARecycler"

exit