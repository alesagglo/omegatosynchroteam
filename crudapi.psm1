Function Get-Synchroteam {
    Param($methode, $url, $body, $bearer)

    $headers= @{ 
        'Content-Type'= 'application/json'
        'Accept'= 'application/json'
        'Authorization'= "Basic $bearer"
        'Cache-Control'= 'no-cache'
    } 
      
    try {
        Invoke-RestMethod -Method GET -Uri $url -Headers $headers
    } catch {
        return $_.Exception
    }
}

Function Post-Synchroteam {
    Param($data, $date, $bearer, $config, $file)

    $headers= @{
        'Content-Type'= 'application/json; charset=utf-8'
        'Accept'= 'application/json'
        'Authorization'= "Basic $bearer"
        'Cache-Control'= 'no-cache'
    } 

    #r�cup�ration id equipement, site et client
    $nameEquipement = $data.'N� de Compteur (CPT)'
    $url = "https://ws.synchroteam.com/api/v3/equipment/details?name=$nameEquipement"
    $typePost = "MAJ SITE/EQT"
    $dejaAJour = $FALSE
    try {
        $response = Invoke-RestMethod -Method GET -Uri $url -Headers $headers
        $idEquipement = $response.id
        $idSite = $response.site.id
        $idClient = $response.customer.id
        $dejaAJour = ($response.dateModified.substring(0,10) -eq $date)
    } catch {
        #equipement inexistant
        $idEquipement = ""
        $typePost = "CREATE EQT"
        $nameSite = $data.'N� du Point de Consommation (PC)'
        $url = "https://ws.synchroteam.com/api/v3/site/details?name=$nameSite"
        try {
            $response = Invoke-RestMethod -Method GET -Uri $url -Headers $headers
            $idSite = $response.id
            $idClient = $response.customer.id
        } catch {
            #site inexistant
            $idSite = ""
            $typePost = "CREATE SITE/EQT"
            $nameClient = $data.'Valeur de la Propri�t� (PRP_PC)'
            $url = "https://ws.synchroteam.com/api/v3/customer/details?name=$nameClient"
            try {
                $response = Invoke-RestMethod -Method GET -Uri $url -Headers $headers
                $idClient = $response.id
            } catch {
                
            }
        }
    }

    $tracelog = @{
        'result' = 'OK'
        'dejaAJour' = $dejaAJour
        'type' = $typePost
        'date' = [datetime]::Today.ToString('yyyy-MM-dd HH:mm')
        'nomEquipement' = $nameEquipement
        'erreur' = $response.error
        'message' = $response.message
        'idEquipement' = $idEquipement
        'idSite' = $idSite
        'idClient' = $idClient
    }

    if ($dejaAJour -eq $FALSE){
        #SITE correspondant au point de consommation
        $url = "https://ws.synchroteam.com/api/v3/site/send"
        $adresseComplement=''
        if (![String]::isNullOrEmpty($data.'Compl�ment d''Adresse (PC)')) {
            $adresseComplement = $data.'Compl�ment d''Adresse (PC)'
        }
        $latitude=''
        $longitude=''
        if (![String]::isNullOrEmpty($data.'Coordonn�es SIG (PC)')) {
            $latitude = $data.'Coordonn�es SIG (PC)'.Split('|')[0]
            $longitude = $data.'Coordonn�es SIG (PC)'.Split('|')[1]
        }
        $secteur12 = ''
        $secteur = $data.'Valeur de la Propri�t� (PRP_PC)'.Split(' ')[0]
        if (($secteur -eq 'S1') -or ($secteur -eq 'S2')) {
            $secteur12 = 'SECTEUR 12'
        }
        $body= @{
            'name'= $data.'N� du Point de Consommation (PC)'
            'addressStreet' = $data.'Adresse (PC)'
            'addressZIP'= $data.'Code Postal (PC)'
            'addressCity'= $data.'Ville (PC)'
            'addressCountry'= 'France'
            'addressComplement'=$adresseComplement 
            'address'=$data.'Adresse (PC)' + ' ' + $data.'Code Postal (PC)' + ' ' + $data.'Ville (PC)'
            'contactLastName'= $data.'Nom de L''Abonn� (CTR)'
            'contactFirstName'= $data.'Pr�nom de l''Abonn� (CTR)'
            'contactPhone'= Format_Tel($data.'T�l�phone (ABO)')
            'contactMobile'= Format_Tel($data.'Mobile (ABO)')
            'contactEmail'= $data.'Courriel (ABO)'
            'customer'= @{id= $idClient}
            'position'= @{longitude=$longitude
                          latitude =$latitude}
            'customFieldValues'= @{ id=$config.champs_personnalises.site.id_etage
                                    value= $data.'Etage (PC)'},
                                 @{ id=$config.champs_personnalises.site.id_num_logement
                                    value= $data.'N� d''Appartement (PC)'},
                                 @{ id=$config.champs_personnalises.site.id_nom_proprietaire
                                    value= $data.'Nom du Propri�taire (PC)'},
                                 @{ id=$config.champs_personnalises.site.id_prenom_proprietaire 
                                    value= $data.'Pr�nom du Propri�taire (PC)'},
                                 @{ id=$config.champs_personnalises.site.id_tel_mobile_proprietaire
                                    value= Format_Tel($data.'Mobile (Prop)')},
                                 @{ id=$config.champs_personnalises.site.id_tel_fixe_proprietaire 
                                    value= Format_Tel($data.'T�l�phone (Prop)')},
                                 @{ id=$config.champs_personnalises.site.id_mail_proprietaire
                                    value= $data.'Mail (Prop)'},
                                 @{ id=$config.champs_personnalises.site.id_num_contrat
                                    value= $data.'N� de Contrat (CTR)'}
            'tags'= ($data.'Valeur de la Propri�t� (PRP_PC)'.Split(' ')[0] -replace "S", "SECTEUR "),
                    'TOUS SECTEURS',
                    $secteur12
        }
        #    @{ id=$config.champs_personnalises.site.id_code_entree
        #                                value= $data.'PCOPL_CODE'},
        if ($idSite -match '^\d+$') {
            $body|Add-Member -MemberType NoteProperty -Name "id" -Value $idSite
        }
    
        $tracelog."site_name"                  = $body.name
        $tracelog."site_addressStreet"         = $body.addressStreet
        $tracelog."site_addressZIP"            = $body.addressZIP
        $tracelog."site_addressCity"           = $body.addressCity
        $tracelog."site_addressCountry"        = $body.addressCountry
        $tracelog."site_addressComplement"     = $body.addressComplement
        $tracelog."site_address"               = $body.address
        $tracelog."site_contactLastName"       = $body.contactLastName
        $tracelog."site_contactFirstName"      = $body.contactFirstName
        $tracelog."site_contactPhone"          = $body.contactPhone
        $tracelog."site_contactMobile"         = $body.contactMobile
        $tracelog."site_contactEmail"          = $body.contactEmail
        $tracelog."site_customerId"            = $body.customer.id
        $tracelog."site_longitude"             = $body.position.longitude
        $tracelog."site_latitude"              = $body.position.latitude
        $tracelog."site_etage"                 = $body.customFieldValues[0].value
        $tracelog."site_numLogement"           = $body.customFieldValues[1].value
        $tracelog."site_nomPropri�taire"       = $body.customFieldValues[2].value
        $tracelog."site_pr�nomPropri�taire"    = $body.customFieldValues[3].value
        $tracelog."site_telMobileProprietaire" = $body.customFieldValues[4].value
        $tracelog."site_telPropri�taire"       = $body.customFieldValues[5].value
        $tracelog."site_mailPropri�taire"      = $body.customFieldValues[6].value
        $tracelog."site_numContrat"            = $body.customFieldValues[7].value
        $tracelog."site_tag1"                  = $body.tags[0]
        $tracelog."site_tag2"                  = $body.tags[1]
        $tracelog."site_tag3"                  = $body.tags[2]
        $tracelog."site_tag4"                  = $body.tags[3]
        $tracelog."site_tag5"                  = $body.tags[4]

        $body = $body | ConvertTo-Json
        $body = [System.Text.Encoding]::UTF8.GetBytes($body)
        try {    
            $response = Invoke-RestMethod -Method POST -Uri $url -Headers $headers -Body $body #-StatusCodeVariable "retour" 
            $idSite = $response.id
            $tracelog.result = "OK" #$retour
        } catch {
            $tracelog.result = $_.Exception
        }

        #$tracelog|Add-Member -MemberType NoteProperty -Name "MAJSITE_retour" -Value $response
  
        if ([STRING]::IsNullOrEmpty($response.error)){
            #Cr�ation de l'�quipement associ� � ce site
            #Cr�ation du SITE correspondant au point de consommation
            $url = "https://ws.synchroteam.com/api/v3/equipment/send"
            $body= @{
                'name'= $data.'N� de Compteur (CPT)'
                'myId' = $null
                'site'= @{id= $idSite}
                'customer'= @{id= $idClient}
                'customFieldValues'= @{ id=$config.champs_personnalises.equipement.id_emplacement1
                                        value= $data.'Dernier Commentaire G�n�ral (PC)'},
                                     @{ id=$config.champs_personnalises.equipement.id_emplacement2
                                        value= $data.'Dernier Commentaire sur le Compteur (PC)'},
                                     @{ id=$config.champs_personnalises.equipement.id_marque_compteur
                                        value= $data.'Marque (CPT)'},
                                     @{ id=$config.champs_personnalises.equipement.id_annee
                                        value= $data.'Ann�e de Fabrication (CPT)'},
                                     @{ id=$config.champs_personnalises.equipement.id_diametre
                                        value= $data.'Diam�tre (CPT)'},
                                     @{ id=$config.champs_personnalises.equipement.id_dernier_index
                                        value= $data.'Nouvel Index r-0 (PC)'},
                                     @{ id=$config.champs_personnalises.equipement.id_date_dernier_index
                                        value= Format_Date($data.'Date Nouvel  Index r-0 (PC)')}
            }
            if ($idEquipement -match '^\d+$') {
                $body|Add-Member -MemberType NoteProperty -Name "id" -Value $idEquipement
            }

            $tracelog."eqpt_name"             = $body.name
            $tracelog."eqpt_customerId"       = $body.customer.id
            $tracelog."eqpt_id"               = $body.id
            $tracelog."eqpt_emplacement1"     = $body.customFieldValues[0].value
            $tracelog."eqpt_emplacement2"     = $body.customFieldValues[1].value
            $tracelog."eqpt_marque"           = $body.customFieldValues[2].value
            $tracelog."eqpt_ann�e"            = $body.customFieldValues[3].value
            $tracelog."eqpt_diam�tre"         = $body.customFieldValues[4].value
            $tracelog."eqpt_dernierIndex"     = $body.customFieldValues[5].value
            $tracelog."eqpt_dateDernierIndex" = $body.customFieldValues[6].value

            $body = $body | ConvertTo-Json  
            $body = [System.Text.Encoding]::UTF8.GetBytes($body)

            try {    
                $response = Invoke-RestMethod -Method POST -Uri $url -Headers $headers -Body $body
                $idEquipment = $response.id
                $tracelog.result = "OK" 
            } catch {
                $tracelog.result = $_.Exception
            }
        }
    }

    [PSCustomObject] $tracelog | Export-Csv -Path ([System.String]::Concat($file, '.csv')) -Append -Delimiter ";" -NoTypeInformation -Encoding UTF8 -Force
    return $response
}